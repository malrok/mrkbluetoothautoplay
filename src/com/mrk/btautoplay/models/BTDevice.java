package com.mrk.btautoplay.models;

import com.mrk.mrkframework.model.ModelInterface;


/**
 * Class describing a Bluetooth device linked to your phone
 * @author Simon
 * Transient
 */
public class BTDevice implements ModelInterface {

	/** 
	 * Device mac address 
	 * Primary key
	 */
	private String btAddress;
	
	/** Device name */
	private String btName;
	
	/** Device class */
	private String btClass;

	public String getBtAddress() {
		return btAddress;
	}

	public void setBtAddress(String btAddress) {
		this.btAddress = btAddress;
	}

	public String getBtName() {
		return btName;
	}

	public void setBtName(String btName) {
		this.btName = btName;
	}

	public String getBtClass() {
		return btClass;
	}

	public void setBtClass(String btClass) {
		this.btClass = btClass;
	}

	@Override
	public BTDevice clone() {
		BTDevice cloned = new BTDevice();
		
		cloned.setBtAddress(this.btAddress);
		cloned.setBtClass(this.btClass);
		cloned.setBtName(this.btName);
		
		return cloned;
	}
}
