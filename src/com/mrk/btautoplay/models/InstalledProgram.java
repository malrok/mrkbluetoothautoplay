package com.mrk.btautoplay.models;

import android.graphics.Bitmap;
import com.mrk.mrkframework.model.ModelInterface;


/**
 * Class describing an installed program
 * @author Simon
 * Transient
 */
public class InstalledProgram implements ModelInterface {

	/** Label of the program as displayed in Android */
	private String ipLabel;
	
	/** Package of the program */
	private String ipPackage;
	
	/** Class of the program */
	private String ipClass;

	/** Icon of the program */
	private Bitmap ipIcon;
	
	public String getIpLabel() {
		return ipLabel;
	}

	public void setIpLabel(String ipLabel) {
		this.ipLabel = ipLabel;
	}

	public String getIpPackage() {
		return ipPackage;
	}

	public void setIpPackage(String ipPackage) {
		this.ipPackage = ipPackage;
	}

	public String getIpClass() {
		return ipClass;
	}

	public void setIpClass(String ipClass) {
		this.ipClass = ipClass;
	}

	public Bitmap getIpIcon() {
		return ipIcon;
	}

	public void setIpIcon(Bitmap ipIcon) {
		this.ipIcon = ipIcon;
	}
	
	@Override
	public InstalledProgram clone() {
		InstalledProgram cloned = new InstalledProgram();
		
		cloned.ipClass = this.ipClass;
		cloned.ipIcon = Bitmap.createBitmap(this.ipIcon, 0, 0, this.ipIcon.getWidth(), this.ipIcon.getHeight(), null, true);
		cloned.ipLabel = this.ipLabel;
		cloned.ipPackage = this.ipPackage;
		
		return cloned;
	}
}
