package com.mrk.btautoplay.models;

import com.mrk.mrkframework.model.ModelInterface;


/**
 * Class describing a program execution to process when a given bluetooth device connects
 * @author Simon
 *
 */
public class Execution implements ModelInterface {
	
	/** Should we try to play the application */
	private boolean exPlay;
	
	/** Address of the linked bluetooth device */
	private String exBtDeviceAddress;
	
	/** Name of the linked bluetooth device */
	private String exBtDeviceName;
	
	/** program to launch */
	private InstalledProgram exProgram;
	
	/** delay before triggering play */
	private long exDelay;

	/** should we maximize the media volume */
	private boolean exMaxVolume;
	
	public Execution() {
		this.exProgram = new InstalledProgram();
	}

	public boolean getExPlay() {
		return exPlay;
	}

	public void setExPlay(boolean exPlay) {
		this.exPlay = exPlay;
	}

	public String getExBtDeviceAddress() {
		return exBtDeviceAddress;
	}

	public void setExBtDeviceAddress(String exBtDeviceAddress) {
		this.exBtDeviceAddress = exBtDeviceAddress;
	}
	
	public String getExBtDeviceName() {
		return exBtDeviceName;
	}

	public void setExBtDeviceName(String exBtDeviceName) {
		this.exBtDeviceName = exBtDeviceName;
	}

	public InstalledProgram getExProgram() {
		return this.exProgram;
	}
	
	public void setExProgram(InstalledProgram exProgram) {
		this.exProgram = exProgram;
	}

	public long getExDelay() {
		return exDelay;
	}

	public void setExDelay(long exDelay) {
		this.exDelay = exDelay;
	}
	
	public boolean getExMaxVolume() {
		return exMaxVolume;
	}

	public void setExMaxVolume(boolean exMaxVolume) {
		this.exMaxVolume = exMaxVolume;
	}

	@Override
	public Execution clone() {
		Execution copy = new Execution();
		copy.exPlay = this.exPlay;
		copy.exBtDeviceAddress = this.exBtDeviceAddress;
		copy.exBtDeviceName = this.exBtDeviceName;
		copy.exProgram = this.exProgram;
		copy.exDelay = this.exDelay;
		copy.exMaxVolume = this.exMaxVolume;
		return copy;
	}
}
