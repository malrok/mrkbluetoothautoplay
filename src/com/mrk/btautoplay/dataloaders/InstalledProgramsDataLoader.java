package com.mrk.btautoplay.dataloaders;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;

import com.mrk.btautoplay.models.InstalledProgram;
import com.mrk.mrkframework.dataloaders.abstracts.AbstractDataLoader;

public class InstalledProgramsDataLoader extends AbstractDataLoader<InstalledProgram> {

	@Override
	public List<InstalledProgram> getData(final Context context) {
		final List<InstalledProgram>data = new ArrayList<InstalledProgram>();
		
		PackageManager pm = context.getPackageManager();
		Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
		mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
		List<ResolveInfo> pgList = pm.queryIntentActivities(mainIntent, 0);

		for (ResolveInfo rInfo : pgList) {
			InstalledProgram program = new InstalledProgram();
			
			program.setIpLabel(rInfo.activityInfo.loadLabel(pm).toString());
			program.setIpPackage(rInfo.activityInfo.packageName);
			program.setIpClass(rInfo.activityInfo.name);
			
			// icon
			Bitmap apkIcon = null;
			
			if (rInfo.activityInfo.loadIcon(pm) instanceof BitmapDrawable) {
				apkIcon = ((BitmapDrawable)rInfo.activityInfo.loadIcon(pm)).getBitmap();
			} else {
			     Bitmap bitmap = Bitmap.createBitmap(rInfo.activityInfo.loadIcon(pm).getIntrinsicWidth(),rInfo.activityInfo.loadIcon(pm).getIntrinsicHeight(), Config.ARGB_8888);
			     Canvas canvas = new Canvas(bitmap); 
			     rInfo.activityInfo.loadIcon(pm).setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
			     rInfo.activityInfo.loadIcon(pm).draw(canvas);
			     apkIcon = bitmap;
			}
			
			program.setIpIcon(apkIcon);
			
			data.add(program);
		}
		
		sortList(context, data);
		
		return data;
	}

	@Override
	public void pushFrom(Context context, List<InstalledProgram> data) {
		
	}

	private void sortList(final Context context, List<InstalledProgram> data) {
		Comparator<InstalledProgram> appsComparator = new Comparator<InstalledProgram>() {
			@Override
			public int compare(InstalledProgram lhs, InstalledProgram rhs) {
				return lhs.getIpLabel().compareToIgnoreCase(rhs.getIpLabel());
			}      
		};
		
		Collections.sort(data,appsComparator);
	}
	
}
