package com.mrk.btautoplay.dataloaders;

import java.util.List;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import com.mrk.btautoplay.R;
import com.mrk.btautoplay.models.Execution;
import com.mrk.mrkframework.dataloaders.abstracts.AbstractDataLoader;

public class ExecutionDataLoader extends AbstractDataLoader<Execution> {
	
	private XMLParser parser = new XMLParser();

	@Override
	public List<Execution> getData(Context context) {
		List<Execution>data = parser.parse(context);
		
		BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
		
		for (Execution exec : data) {
			try {
				Drawable icon = context.getPackageManager().getApplicationIcon(exec.getExProgram().getIpPackage());
				exec.getExProgram().setIpIcon(((BitmapDrawable)icon).getBitmap());
			} catch (NameNotFoundException e) {
				e.printStackTrace();
				exec.getExProgram().setIpIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.no_icon));
			}

			try {
				exec.setExBtDeviceName(adapter.getRemoteDevice(exec.getExBtDeviceAddress()).getName());
			} catch(IllegalArgumentException e) {
				e.printStackTrace();
				exec.setExBtDeviceName("");
			}
			
		}
		
		return data;
	}

	@Override
	public void pushFrom(Context context, List<Execution> data) {
		parser.unparse(context, data);
	}

}
