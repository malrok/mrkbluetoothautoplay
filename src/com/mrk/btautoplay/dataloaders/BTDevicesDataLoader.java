package com.mrk.btautoplay.dataloaders;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;

import com.mrk.btautoplay.models.BTDevice;
import com.mrk.mrkframework.dataloaders.abstracts.AbstractDataLoader;

public class BTDevicesDataLoader extends AbstractDataLoader<BTDevice> {
	
	@Override
	public List<BTDevice> getData(Context context) {
		BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
		
		List<BTDevice> data = new ArrayList<BTDevice>();
		
		if(adapter!=null) {
			adapter.startDiscovery();		
			
			for (BluetoothDevice btDevice : adapter.getBondedDevices()) {
				BTDevice device = new BTDevice();
				device.setBtAddress(btDevice.getAddress());
				device.setBtClass(btDevice.getClass().toString());
				device.setBtName(btDevice.getName());
				data.add(device);
			}
		}
		
		sortList(context, data);
		
		return data;
	}

	@Override
	public void pushFrom(Context context, List<BTDevice> data) {
		
	}
	
	private void sortList(final Context context, List<BTDevice> data) {
		Comparator<BTDevice> appsComparator = new Comparator<BTDevice>() {
			@Override
			public int compare(BTDevice lhs, BTDevice rhs) {
				return lhs.getBtName().compareToIgnoreCase(rhs.getBtName());
			}      
		};
		
		Collections.sort(data,appsComparator);
	}
}
