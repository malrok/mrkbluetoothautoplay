package com.mrk.btautoplay.dataloaders;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.content.Context;
import android.util.Xml;

import com.mrk.btautoplay.models.Execution;
import com.mrk.mrkframework.dataloaders.abstracts.AbstractFileParser;

public class XMLParser extends AbstractFileParser<Execution> {

	private static int FILE_VERSION = 1;
	public static String SETTINGS_FILE = "options.xml";
	
	public XMLParser() {
		this.file = SETTINGS_FILE;
	}
	
	@Override
	public List<Execution> parse(Context context) {
		List<Execution> result = new ArrayList<Execution>();
		int fileVersion = 0;
		XmlPullParser parser = Xml.newPullParser();
		
		try {
			FileInputStream fIn = context.openFileInput(this.file); 
			InputStreamReader isr = new InputStreamReader(fIn);
			
			// auto-detect the encoding from the stream
			parser.setInput(isr);
			int eventType = parser.getEventType();

			Execution currentExecution = null;
			
			while (eventType != XmlPullParser.END_DOCUMENT){
				String name = null;
				switch (eventType){
					case XmlPullParser.START_DOCUMENT:
						break;
					case XmlPullParser.START_TAG:
						name = parser.getName();
						if (name.equalsIgnoreCase("Options")) {
							for (int i = 0; i < parser.getAttributeCount(); i++) {
								if (parser.getAttributeName(i) != null) {
									if (parser.getAttributeName(i).equalsIgnoreCase("FileVersion")) {
										fileVersion = Integer.parseInt(parser.getAttributeValue(i));
									}
								}
							}
						}
						if (name.equalsIgnoreCase("OptionLine")) {
							currentExecution = new Execution();
							for (int i=0 ; i<parser.getAttributeCount() ; i++) {
								if (parser.getAttributeName(i)!=null) {
									if (parser.getAttributeName(i).equalsIgnoreCase("BTDevice")) {
										currentExecution.setExBtDeviceAddress(parser.getAttributeValue(i));
									}
									if (parser.getAttributeName(i).equalsIgnoreCase("PGName")) {
										currentExecution.getExProgram().setIpLabel(parser.getAttributeValue(i));
									}
									if (parser.getAttributeName(i).equalsIgnoreCase("PGPackage")) {
										currentExecution.getExProgram().setIpPackage(parser.getAttributeValue(i));
									}
									if (parser.getAttributeName(i).equalsIgnoreCase("PGClass")) {
										currentExecution.getExProgram().setIpClass(parser.getAttributeValue(i));
									}
									if (parser.getAttributeName(i).equalsIgnoreCase("PGPlay")) {
										currentExecution.setExPlay(parser.getAttributeValue(i).equalsIgnoreCase("yes"));
									}
									if (parser.getAttributeName(i).equalsIgnoreCase("PGPlayDelay")) {
										currentExecution.setExDelay(Integer.parseInt(parser.getAttributeValue(i)));
									}
									if (parser.getAttributeName(i).equalsIgnoreCase("PGMaxVolume")) {
										currentExecution.setExMaxVolume(parser.getAttributeValue(i).equalsIgnoreCase("yes"));
									}
								}
							}
						}
						break;
					case XmlPullParser.END_TAG:
						name = parser.getName();
						if (name.equalsIgnoreCase("OptionLine") && currentExecution != null){
							result.add(currentExecution);
						}
						break;
				}
				eventType = parser.next();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (XmlPullParserException e) {
			e.printStackTrace();
		}
		
		while (fileVersion < FILE_VERSION) {
			migrateFile(fileVersion++, result);
		}
		
		return result;
	}

	@Override
	public void unparse(Context context, List<Execution> objects) {
		try {
			FileOutputStream fOut = context.openFileOutput(this.file, Context.MODE_PRIVATE); 
			OutputStreamWriter osw = new OutputStreamWriter(fOut);
			
			String fileContent = "<?xml version='1.0' encoding='utf-8'?>\n";
			
			fileContent += "<Options \tFileVersion=\"" + FILE_VERSION + "\">\n";
			
			for (Execution exec : objects) {
				fileContent += "<OptionLine ";
				
				fileContent += "\t BTDevice=\"" + exec.getExBtDeviceAddress() + "\"";
				fileContent += "\t PGName=\"" + exec.getExProgram().getIpLabel() + "\"";
				fileContent += "\t PGPackage=\"" + exec.getExProgram().getIpPackage() + "\"";
				fileContent += "\t PGClass=\"" + exec.getExProgram().getIpClass() + "\"";				
				fileContent += "\t PGPlay=\"" + (exec.getExPlay() ? "yes" : "no") + "\"";
				fileContent += "\t PGPlayDelay=\"" + exec.getExDelay() + "\"";
				fileContent += "\t PGMaxVolume=\"" + (exec.getExMaxVolume() ? "yes" : "no") + "\"";

				fileContent += "\t/>\n";
			}
			
			fileContent += "</Options>";
			
			if (fileContent!=null) {
				osw.write(fileContent); 
				osw.flush(); 
				osw.close();
			}
		} catch (FileNotFoundException e) {

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void migrateFile(int fileVersion, List<Execution> execs) {
		if (fileVersion == 0) {
			for (Execution exec : execs) {
				exec.setExPlay(false);
				exec.setExMaxVolume(false);
				exec.setExDelay(0);
			}
		}
	}
	
}
