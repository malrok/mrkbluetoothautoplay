package com.mrk.btautoplay.screens;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;

import com.mrk.btautoplay.R;
import com.mrk.btautoplay.screens.adapters.BTDevicesListAdapter;
import com.mrk.btautoplay.screens.adapters.InstalledProgramAdapter;
import com.mrk.btautoplay.screens.helper.DetailStatus;
import com.mrk.btautoplay.viewmodel.BTDevicesListViewModel;
import com.mrk.btautoplay.viewmodel.ExecutionViewModel;
import com.mrk.btautoplay.viewmodel.InstalledProgramsListViewModel;
import com.mrk.mrkframework.viewmodel.interfaces.IListViewModel;
import com.mrk.mrkframework.viewmodel.interfaces.OnListViewModelLoadingListener;

public class ExecutionDetailFragment extends Fragment implements OnItemSelectedListener, OnListViewModelLoadingListener {

	/** widgets */
	private Spinner devicePicker;
	private Spinner programPicker;
	private ProgressBar programPickerProgress;
	private CheckBox shouldPlay;
	private LinearLayout playLayout;
	private SeekBar playDelay;
	private TextView playDelayText;
	private CheckBox maximizeVolume;
	
	/** adapters */
	private BTDevicesListAdapter btAdapter;
	private InstalledProgramAdapter ipAdapter;
	
	/** viewmodels */
	public ExecutionViewModel execViewModel;
	private BTDevicesListViewModel btViewModels;
	private InstalledProgramsListViewModel ipViewModels;
	
	/** variables */
	public DetailStatus status = DetailStatus.VALIDATED;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.activity_execution_layout, container, false);
		
		devicePicker = (Spinner) view.findViewById(R.id.btdevice_picker);
		programPicker = (Spinner) view.findViewById(R.id.program_picker);
		programPickerProgress = (ProgressBar) view.findViewById(R.id.program_picker_progress);
		shouldPlay = (CheckBox) view.findViewById(R.id.execution_play);
		playLayout = (LinearLayout) view.findViewById(R.id.play_layout);
		playDelay = (SeekBar) view.findViewById(R.id.execution_delay);
		playDelayText = (TextView) view.findViewById(R.id.execution_delay_text);
		maximizeVolume = (CheckBox) view.findViewById(R.id.execution_maxvolume);
		
		if (ipViewModels.isLoading()) {
			programPicker.setEnabled(false);
			programPickerProgress.setVisibility(View.VISIBLE);
			ipViewModels.setOnListViewModelLoadingListener(this);
		} else {
			this.ipAdapter = new InstalledProgramAdapter(getActivity().getApplicationContext(), R.layout.item_btdevice_picker, this.ipViewModels.getData());
			this.ipAdapter.notifyDataSetChanged();
		}
		
		this.btAdapter = new BTDevicesListAdapter(getActivity().getApplicationContext(), R.layout.item_btdevice_picker, this.btViewModels.getData());
		
		return view;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		if (activity instanceof MainScreen) {
			btViewModels = ((MainScreen) activity).btViewModels;
			ipViewModels = ((MainScreen) activity).ipViewModels;
			execViewModel = ((MainScreen) activity).getSelectedItem();
		} else {
			throw new ClassCastException(activity.toString() + " must implemenet MainScreen");
		}
	}
	
	@Override
	public void onResume() {
		super.onResume();
		devicePicker.setAdapter(btAdapter);
		devicePicker.setSelection(this.btViewModels.getRankByAddress(execViewModel.getData().getExBtDeviceAddress()));
		devicePicker.setOnItemSelectedListener(this);
		
		programPicker.setAdapter(ipAdapter);
		programPicker.setSelection(this.ipViewModels.getRankBy(execViewModel.getData().getExProgram()));
		programPicker.setOnItemSelectedListener(this);
		
		shouldPlay.setChecked(this.execViewModel.getData().getExPlay());
		shouldPlay.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				execViewModel.getData().setExPlay(isChecked);
				playLayout.setVisibility((shouldPlay.isChecked() ? View.VISIBLE : View.GONE));
			}
		});
		
		playLayout.setVisibility((shouldPlay.isChecked() ? View.VISIBLE : View.GONE));
		
		playDelay.setProgress((int) this.execViewModel.getData().getExDelay());
		playDelay.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				execViewModel.getData().setExDelay(progress);
				playDelayText.setText(String.valueOf(playDelay.getProgress()));
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) { }

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) { }
		});
		
		playDelayText.setText(String.valueOf(playDelay.getProgress()));
		
		maximizeVolume.setChecked(this.execViewModel.getData().getExMaxVolume());
		maximizeVolume.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				execViewModel.getData().setExMaxVolume(isChecked);
			}
		});
	}
	
	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
		if (parent.equals(devicePicker)) {
			this.execViewModel.getData().setExBtDeviceAddress(this.btViewModels.getByRank(position).getBtAddress());
			this.execViewModel.getData().setExBtDeviceName(this.btViewModels.getByRank(position).getBtName());
		} else if (parent.equals(programPicker)) {
			this.execViewModel.getData().setExProgram(this.ipViewModels.getByRank(position));
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// nothing to do
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public void finishedLoading(IListViewModel listViewModel) {
		if (getActivity() != null && listViewModel.equals(ipViewModels)) {
			getActivity().runOnUiThread(new Runnable() {
			    public void run() {
					programPicker.setEnabled(true);
					programPickerProgress.setVisibility(View.GONE);
					ipAdapter = new InstalledProgramAdapter(getActivity().getApplicationContext(), R.layout.item_btdevice_picker, ipViewModels.getData());
					ipAdapter.notifyDataSetChanged();
					programPicker.setAdapter(ipAdapter);
					programPicker.setSelection(ipViewModels.getRankBy(execViewModel.getData().getExProgram()));
			    }
			});
		}
	}

	public void setStatus(DetailStatus status) {
		this.status = status;
	}

}
