package com.mrk.btautoplay.screens;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.mrk.btautoplay.R;
import com.mrk.btautoplay.dataloaders.XMLParser;

public class OptionFragment extends Fragment implements OnClickListener {

	private Button importButton;
	private Button exportButton;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.activity_options_layout, container, false);
		
		importButton = (Button) view.findViewById(R.id.import_button);
		exportButton = (Button) view.findViewById(R.id.export_button);
		
		importButton.setOnClickListener(this);
		exportButton.setOnClickListener(this);
		
		return view;
	}

	@Override
	public void onClick(View view) {
		final String orig = getActivity().getApplicationContext().getFilesDir().getPath() + "/" + XMLParser.SETTINGS_FILE;
		final String dest = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + XMLParser.SETTINGS_FILE;
		
		if (view.equals(importButton)) {
			new AlertDialog.Builder(getActivity()).setIcon(android.R.drawable.ic_dialog_alert).setTitle(R.string.import_text)
					.setMessage(R.string.import_text_summary).setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							if (copy(dest, orig))
								Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.import_ok), Toast.LENGTH_LONG).show();
							else
								Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.import_nok), Toast.LENGTH_LONG).show();
						}

						}).setNegativeButton(R.string.no, null).show();
		} else if (view.equals(exportButton)) {
			File file = new File(dest);
			if (file.exists()) {
				new AlertDialog.Builder(getActivity()).setIcon(android.R.drawable.ic_dialog_alert).setTitle(R.string.export_text)
						.setMessage(R.string.file_exists).setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								export(orig, dest);
							}
						}).setNegativeButton(R.string.no, null).show();
			} else
				export(orig, dest);
		}
		
	}
	
	private void export(String orig, String dest) {
		if (copy(orig, dest)) {
			Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.export_ok), Toast.LENGTH_LONG).show();
		} else {
			Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.export_nok), Toast.LENGTH_LONG).show();
		}
	}
	
	private boolean copy(String src, String dst) {
		boolean ok = true;
		try {
			InputStream in = new FileInputStream(src);
			OutputStream out = new FileOutputStream(dst);

			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			in.close();
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			ok = false;
		} catch (IOException e) {
			e.printStackTrace();
			ok = false;
		}

		return ok;
	}
	
}
