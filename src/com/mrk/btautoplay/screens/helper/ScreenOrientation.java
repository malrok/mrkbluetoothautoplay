package com.mrk.btautoplay.screens.helper;

public enum ScreenOrientation {
	PORTRAIT,
	LANDSCAPE;
}
