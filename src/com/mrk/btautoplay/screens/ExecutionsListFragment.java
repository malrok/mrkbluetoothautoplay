package com.mrk.btautoplay.screens;

import android.app.Activity;
import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.mrk.btautoplay.R;
import com.mrk.btautoplay.screens.adapters.ExecutionsListAdapter;
import com.mrk.btautoplay.viewmodel.ExecutionsListViewModel;

public class ExecutionsListFragment extends ListFragment {

	/** view model */
	private ExecutionsListViewModel listViewModel;

	/** list adapter */
	private ExecutionsListAdapter adapter;
	
	/** listener */
	private OnItemSelectedListener listener;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.activity_execution_list_layout, container, false);
		
		return view;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		if (activity instanceof MainScreen) {
			this.listener = (OnItemSelectedListener) activity;
			this.listViewModel = ((MainScreen)activity).execsViewModel;
			this.adapter = new ExecutionsListAdapter(activity.getApplicationContext(), R.layout.item_execution_layout, listViewModel.getListData());
		} else {
			throw new ClassCastException(activity.toString() + " must implemenet MainScreen");
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		setListAdapter(adapter);
	}
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		listViewModel.setSelectedItem(position);
		listener.onExecutionItemSelected();
	}

	public void reload(Activity activity) {
		this.adapter = new ExecutionsListAdapter(activity.getApplicationContext(), R.layout.item_execution_layout, listViewModel.getListData());
		this.adapter.notifyDataSetChanged();
	}
	
	public interface OnItemSelectedListener {
		public void onExecutionItemSelected();
	}
}
