package com.mrk.btautoplay.screens;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.mrk.btautoplay.R;
import com.mrk.btautoplay.dataloaders.BTDevicesDataLoader;
import com.mrk.btautoplay.dataloaders.ExecutionDataLoader;
import com.mrk.btautoplay.dataloaders.InstalledProgramsDataLoader;
import com.mrk.btautoplay.screens.ExecutionsListFragment.OnItemSelectedListener;
import com.mrk.btautoplay.screens.helper.DetailStatus;
import com.mrk.btautoplay.screens.helper.ScreenOrientation;
import com.mrk.btautoplay.viewmodel.BTDevicesListViewModel;
import com.mrk.btautoplay.viewmodel.ExecutionViewModel;
import com.mrk.btautoplay.viewmodel.ExecutionsListViewModel;
import com.mrk.btautoplay.viewmodel.InstalledProgramsListViewModel;

public class MainScreen extends FragmentActivity implements OnItemSelectedListener {

	private static final int REQUEST_ENABLE_BT = 1;
	
	/** viewmodels */
	public ExecutionsListViewModel execsViewModel = new ExecutionsListViewModel();
	public BTDevicesListViewModel btViewModels = new BTDevicesListViewModel();
	public InstalledProgramsListViewModel ipViewModels = new InstalledProgramsListViewModel();
	
	/** menu items */
	private MenuItem addItem;
	private MenuItem deleteItem;
	private MenuItem cancelItem;
	private MenuItem optionsItem;
	
	/** action bar */
	private ActionBar actionBar;
	
	/** fragments */
	// TODO : g�rer les modes portrait / paysage "proprement"
	private ExecutionsListFragment listFragment;
	private ExecutionDetailFragment detailFragment;
	private OptionFragment optionFragment;
	private Fragment currentFragment;
	
	/** variables */
	private ScreenOrientation orientation;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_layout);
        
        // enable the home button
        actionBar = getActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setHomeButtonEnabled(false);
    }

	@Override
	public void onResume() {
		super.onResume();
		
		if (checkBluetoothOn()) {
	        loadData();
		}
		
		setFragments();
	}
	
	@Override
	public void onPause() {
		super.onPause();		
		saveData();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu, menu);

		addItem = menu.findItem(R.id.add);
		deleteItem = menu.findItem(R.id.delete);
		cancelItem = menu.findItem(R.id.cancel);
		optionsItem = menu.findItem(R.id.options);
		
		addItem.setVisible(true);
		deleteItem.setVisible(false);
		cancelItem.setVisible(false);
		optionsItem.setVisible(true);
		
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				addItem.setVisible(true);
				deleteItem.setVisible(false);
				cancelItem.setVisible(false);
				optionsItem.setVisible(true);
				actionBar.setDisplayHomeAsUpEnabled(false);
				actionBar.setHomeButtonEnabled(false);
				changeFragment(listFragment);
				break;
			case R.id.options:
				addItem.setVisible(false);
				deleteItem.setVisible(false);
				cancelItem.setVisible(false);
				optionsItem.setVisible(false);
				actionBar.setDisplayHomeAsUpEnabled(true);
				actionBar.setHomeButtonEnabled(true);
				changeFragment(optionFragment);
				break;
			case R.id.add:
				addItem.setVisible(false);
				deleteItem.setVisible(false);
				cancelItem.setVisible(true);
				optionsItem.setVisible(true);
				execsViewModel.setSelectedItem(-1);
				detailFragment.setStatus(DetailStatus.VALIDATED);
//				loadDetailWithVM(new ExecutionViewModel());
				loadDetail();
				break;
			case R.id.delete:
				addItem.setVisible(true);
				deleteItem.setVisible(false);
				cancelItem.setVisible(false);
				optionsItem.setVisible(true);
				actionBar.setDisplayHomeAsUpEnabled(false);
				actionBar.setHomeButtonEnabled(false);
				detailFragment.setStatus(DetailStatus.DELETED);
				changeFragment(listFragment);
				break;
			case R.id.cancel:
				addItem.setVisible(true);
				deleteItem.setVisible(false);
				cancelItem.setVisible(false);
				optionsItem.setVisible(true);
				actionBar.setDisplayHomeAsUpEnabled(false);
				actionBar.setHomeButtonEnabled(false);
				detailFragment.setStatus(DetailStatus.CANCELLED);
				changeFragment(listFragment);
				break;
		}
		return true;
	}

	@Override
	 public boolean onKeyDown(int keyCode, KeyEvent event) {
		 if (keyCode == KeyEvent.KEYCODE_BACK) {
		    if (getFragmentManager().getBackStackEntryCount() == 0) {
		        this.finish();
		        return false;
		    } else {
		    	addItem.setVisible(true);
				deleteItem.setVisible(false);
				cancelItem.setVisible(false);
				optionsItem.setVisible(true);
				actionBar.setDisplayHomeAsUpEnabled(false);
				actionBar.setHomeButtonEnabled(false);
				changeFragment(listFragment);
	
		        return false;
		    }
		 }
		 return super.onKeyDown(keyCode, event);
	}
	
	@Override
	public void onExecutionItemSelected() {
		addItem.setVisible(false);
		deleteItem.setVisible(true);
		optionsItem.setVisible(true);
//		loadDetailWithVM((ExecutionViewModel) execsViewModel.getSelectedItem().clone());
		loadDetail();
		detailFragment.setStatus(DetailStatus.VALIDATED);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_ENABLE_BT) {
			if (resultCode == RESULT_OK) {
				loadData();
			} else {
				this.finish();
			}
		}
	}
	
	public ExecutionViewModel getSelectedItem() {
		if (execsViewModel.getSelectedItem() == null) {
			return new ExecutionViewModel();
		} else {
			return (ExecutionViewModel) execsViewModel.getSelectedItem().clone();
		}
	}
	
	private boolean checkBluetoothOn() {
		final BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		
		if (bluetoothAdapter == null) {
			// no bluetooth on device
			Toast.makeText(this, getResources().getString(R.string.no_bluetooth), Toast.LENGTH_SHORT).show();
			finish();
		} else {
			if (!bluetoothAdapter.isEnabled()) {
				Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
	            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
			} else {
				return true;
			}
		}
		
		return false;
	}
	
	private void onExitDetail(ExecutionViewModel viewModel, DetailStatus status) {
		if (status == DetailStatus.VALIDATED) {
			if (execsViewModel.getSelectedItem() == null) {
				// on est entrain d'ajouter un viewmodel
				execsViewModel.getListData().add(viewModel);
			} else {
				// on met � jour un viewmodel existant
				((ExecutionViewModel) execsViewModel.getSelectedItem()).loadFromData(viewModel.getData());
			}
		} else if (status == DetailStatus.DELETED) {
			execsViewModel.setSelectedItem(-1);
			execsViewModel.removeVM(viewModel);
		}
		listFragment.reload(this);
	}
	
	private void loadDetail() {
		if (orientation == ScreenOrientation.PORTRAIT) {
			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.setHomeButtonEnabled(true);
			changeFragment(detailFragment);
		}
		
//		detailFragment.setViewModel(vm);
	}

	private void setFragments() {
		detailFragment = (ExecutionDetailFragment) getFragmentManager().findFragmentById(R.id.detailFragment);
		if (detailFragment == null) {
			// on est en portrait
			orientation = ScreenOrientation.PORTRAIT;
			detailFragment = new ExecutionDetailFragment();
			listFragment = new ExecutionsListFragment();
			optionFragment = new OptionFragment();
			
			changeFragment(listFragment);
		} else {
			orientation = ScreenOrientation.LANDSCAPE;
			listFragment = (ExecutionsListFragment) getFragmentManager().findFragmentById(R.id.listFragment);
		}
	}
	
	private void changeFragment(Fragment fragment) {
		if (orientation == ScreenOrientation.PORTRAIT) {
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
			
			getFragmentManager().popBackStack();
			
			if (fragment != listFragment) {
				ft.addToBackStack(null);
			}
			
			// on change de fragment depuis un d�tail
			if (currentFragment != null && currentFragment.equals(detailFragment)) {
				onExitDetail(detailFragment.execViewModel, detailFragment.status);
			}
			
			ft.replace(R.id.fragment_container, fragment);
			ft.commit();
			
			currentFragment = fragment;
		}
	}
	
	/** loads data and inits viewmodels */
	private void loadData() {
		execsViewModel.clear();
		execsViewModel.loadFromDataLoader(getApplicationContext(), new ExecutionDataLoader());
		btViewModels.clear();
		btViewModels.loadFromDataLoader(getApplicationContext(), new BTDevicesDataLoader());
		ipViewModels.clear();
		ipViewModels.loadFromDataLoader(getApplicationContext(), new InstalledProgramsDataLoader());
		
		checkBluetoothPairedDevices();
	}

	private void checkBluetoothPairedDevices() {
		if (btViewModels.getData().size() == 0) {
			new AlertDialog.Builder(this)
				.setIcon(android.R.drawable.ic_dialog_alert).setTitle(R.string.no_paired_devices_title)
				.setMessage(R.string.no_paired_devices).setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						MainScreen.this.finish();
					}

				}).show();
		}
	}

	private void saveData() {
		new ExecutionDataLoader().pushFrom(getApplicationContext(), execsViewModel.getData());
	}
	
}
