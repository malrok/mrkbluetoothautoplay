package com.mrk.btautoplay.screens.adapters;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mrk.btautoplay.R;
import com.mrk.btautoplay.models.InstalledProgram;

public class InstalledProgramAdapter extends BaseAdapter {
	
	private LayoutInflater inflater;
	private List<InstalledProgram> dataList;
	
	public InstalledProgramAdapter(Context context, int resourceId, List<InstalledProgram> dataList) {
		inflater = LayoutInflater.from(context);
		
		this.dataList = dataList;
	}

	@Override
	public int getCount() {
		return dataList.size();
	}
	
	@Override
	public InstalledProgram getItem(int position) {
	    return dataList.get(position);
	}
	
	@Override
	public long getItemId(int position) {
	    return position;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder holder;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.item_program_picker, parent, false);
			
			holder = new ViewHolder();
			holder.image = (ImageView) convertView.findViewById(R.id.program_icon);
			holder.application = (TextView) convertView.findViewById(R.id.program_title);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.image.setImageBitmap(dataList.get(position).getIpIcon());
		holder.application.setText(dataList.get(position).getIpLabel());
		
		return convertView;
	}

	private class ViewHolder {
    	TextView application;
    	ImageView image;
    }
}
