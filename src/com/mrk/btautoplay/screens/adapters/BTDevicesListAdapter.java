package com.mrk.btautoplay.screens.adapters;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mrk.btautoplay.R;
import com.mrk.btautoplay.models.BTDevice;

public class BTDevicesListAdapter  extends BaseAdapter {

	private LayoutInflater inflater;
	private List<BTDevice> dataList;
	
	public BTDevicesListAdapter(Context context, int resourceId, List<BTDevice> dataList) {
		inflater = LayoutInflater.from(context);
		
		this.dataList = dataList;
	}

	@Override
	public int getCount() {
		return dataList.size();
	}
	
	@Override
	public BTDevice getItem(int position) {
	    return dataList.get(position);
	}
	
	@Override
	public long getItemId(int position) {
	    return position;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder holder;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.item_btdevice_picker, parent, false);
			
			holder = new ViewHolder();
			holder.name = (TextView) convertView.findViewById(R.id.btdevice_name);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.name.setText(dataList.get(position).getBtName());
		
		return convertView;
	}
	
	private class ViewHolder {
    	TextView name;
    }

}
