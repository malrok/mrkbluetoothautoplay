package com.mrk.btautoplay.screens.adapters;

import java.util.List;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mrk.btautoplay.R;
import com.mrk.btautoplay.models.Execution;
import com.mrk.mrkframework.viewmodel.interfaces.IViewModel;

public class ExecutionsListAdapter extends ArrayAdapter<Execution> {

	private Context context;
	private LayoutInflater inflater;
	private List<IViewModel<Execution>> dataList;
	
	public ExecutionsListAdapter(Context context, int resourceId, List<IViewModel<Execution>> dataList) {
		super(context, resourceId);
		
		this.context = context;
		inflater = LayoutInflater.from(context);
		
		this.dataList = dataList;
	}

	@Override
	public int getCount() {
		return dataList.size();
	}
	
	@Override
	public Execution getItem(int position) {
	    return dataList.get(position).getData();
	}
	
	@Override
	public long getItemId(int position) {
	    return position;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder holder;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.item_execution_layout, parent, false);
			
			holder = new ViewHolder();
			holder.image = (ImageView) convertView.findViewById(R.id.execution_image);
			holder.application = (TextView) convertView.findViewById(R.id.execution_application);
			holder.action = (TextView) convertView.findViewById(R.id.execution_action);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.image.setImageBitmap(dataList.get(position).getData().getExProgram().getIpIcon());
		holder.application.setText(dataList.get(position).getData().getExProgram().getIpLabel());
		
		String actionText = (dataList.get(position).getData().getExPlay() ? context.getResources().getString(R.string.play) : context.getResources().getString(R.string.execute));
		
		actionText = actionText.replaceAll("@", (dataList.get(position).getData().getExBtDeviceName() == null ? "" : dataList.get(position).getData().getExBtDeviceName()));
		
		holder.action.setText(Html.fromHtml(actionText));
		
		return convertView;
	}

	private class ViewHolder {
    	TextView application, action;
    	ImageView image;
    }
}
