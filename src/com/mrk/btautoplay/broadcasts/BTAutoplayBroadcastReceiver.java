package com.mrk.btautoplay.broadcasts;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.mrk.btautoplay.processors.ExecutionProcessor;

public class BTAutoplayBroadcastReceiver extends BroadcastReceiver {

	private ExecutionProcessor execProcessor;

	@Override
	public void onReceive(Context context, Intent intent) {
		if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(intent.getAction())) {
			BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

			if (execProcessor == null) {
				execProcessor = new ExecutionProcessor(context);
			}

			execProcessor.process(context, device.getAddress());
		}
		
		// DEBUG
		if (intent.getAction().equals("mrk.com.test.playthefuckingmusic")) {
        	
			if (execProcessor == null) {
				execProcessor = new ExecutionProcessor(context);
			}

			execProcessor.process(context, "00:08:e0:31:e0:93");
        }
	}

}
