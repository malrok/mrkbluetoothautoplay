package com.mrk.btautoplay.processors;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.view.KeyEvent;
import android.widget.Toast;

import com.mrk.btautoplay.R;
import com.mrk.btautoplay.dataloaders.ExecutionDataLoader;
import com.mrk.btautoplay.models.Execution;
import com.mrk.btautoplay.models.InstalledProgram;
import com.mrk.btautoplay.viewmodel.ExecutionsListViewModel;

public class ExecutionProcessor {

	private ExecutionsListViewModel execsViewModel = new ExecutionsListViewModel();
	
	public ExecutionProcessor(Context context) {
		execsViewModel.loadFromDataLoader(context, new ExecutionDataLoader());
	}

	public void process(Context context, String macAddress) {
		for (Execution exec : execsViewModel.getData()) {
			if (exec.getExBtDeviceAddress().equalsIgnoreCase(macAddress)) {
				execute(context, exec);
			}
		}
	}

	private void execute(Context context, Execution exec) {
		// launch program
		launch(context, exec.getExProgram());

		// if play
		if (exec.getExPlay()) {
			// wait
			try {
				Thread.sleep(exec.getExDelay() * 1000); // milliseconds
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			// set volume
			if (exec.getExMaxVolume()) {
				AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
				audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
			}
			// play
			if (exec.getExPlay()) {
				play(context, exec.getExProgram().getIpPackage());
			}
		}
	}

	private void play(final Context context, final String appPackage) {
		Intent downIntent = new Intent(Intent.ACTION_MEDIA_BUTTON);
		downIntent.putExtra(Intent.EXTRA_KEY_EVENT, new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE));
		downIntent.setPackage(appPackage);
		context.sendOrderedBroadcast(downIntent, null);

		Intent upIntent = new Intent(Intent.ACTION_MEDIA_BUTTON);
		upIntent.putExtra(Intent.EXTRA_KEY_EVENT, new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE));
		upIntent.setPackage(appPackage);
		context.sendOrderedBroadcast(upIntent, null);
	}

	private void launch(Context context, InstalledProgram exProgram) {
		try {
			Intent startupIntent = new Intent();
			ComponentName distantActivity = new ComponentName(
					exProgram.getIpPackage(), exProgram.getIpClass());
			startupIntent.setComponent(distantActivity);
			startupIntent.setAction(Intent.ACTION_MAIN);
			startupIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(startupIntent);
		} catch (Exception e) {
			Toast.makeText(context, context.getResources().getString(R.string.start_program_error), Toast.LENGTH_LONG).show();
		}
	}

}
