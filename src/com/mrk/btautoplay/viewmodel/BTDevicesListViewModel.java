package com.mrk.btautoplay.viewmodel;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;

import com.mrk.btautoplay.models.BTDevice;
import com.mrk.mrkframework.dataloaders.interfaces.IDataLoader;
import com.mrk.mrkframework.viewmodel.abstracts.AbstractListViewModel;
import com.mrk.mrkframework.viewmodel.interfaces.IViewModel;

public class BTDevicesListViewModel extends AbstractListViewModel<BTDevice> {

	@Override
	public void clear() {
		this.listData = new ArrayList<IViewModel<BTDevice>>();
	}
	
	@Override
	public void loadFromDataLoader(Context context, IDataLoader<BTDevice> dataLoader) {
		List<BTDevice> dataList = dataLoader.getData(context);
		
		for (BTDevice data : dataList) {
			BTDeviceViewModel vm = new BTDeviceViewModel();
			vm.loadFromData(data);
			this.listData.add(vm);
		}
	}
	
	public BTDevice getByAddress(String address) {
		for (IViewModel<BTDevice> vm : this.listData) {
			if (vm.getData().getBtAddress().equalsIgnoreCase(address)) {
				return vm.getData();
			}
		}
		return null;
	}
	
	public BTDevice getByRank(int rank) {
		if (rank >= 0 && rank < this.listData.size()) {
			return this.listData.get(rank).getData();
		}
		return null;
	}
	
	public int getRankByAddress(String address) {
		for (int rank = 0; rank < this.listData.size(); rank++) {
			if (this.listData.get(rank).getData().getBtAddress().equalsIgnoreCase(address)) {
				return rank;
			}
		}
		return 0;
	}
	
}
