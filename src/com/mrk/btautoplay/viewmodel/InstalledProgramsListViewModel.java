package com.mrk.btautoplay.viewmodel;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;

import com.mrk.btautoplay.models.InstalledProgram;
import com.mrk.mrkframework.dataloaders.interfaces.IDataLoader;
import com.mrk.mrkframework.viewmodel.abstracts.AbstractListViewModel;
import com.mrk.mrkframework.viewmodel.interfaces.IViewModel;

public class InstalledProgramsListViewModel extends AbstractListViewModel<InstalledProgram> {

	public InstalledProgramsListViewModel() {
		super();
		isAync = true;
	}
	
	@Override
	public void clear() {
		this.listData = new ArrayList<IViewModel<InstalledProgram>>();
	}
	
	@Override
	public List<InstalledProgram> getData() {
		List<InstalledProgram> data = new ArrayList<InstalledProgram>();
		for (IViewModel<InstalledProgram> vm : listData) {
			if (vm != null) {
				data.add(vm.getData());
			}
		}
		return data;
	}
	
	@Override
	public void loadFromDataLoader(final Context context, final IDataLoader<InstalledProgram> dataLoader) {
		new Thread() {
			public void run() {
				isLoading = true;
				
				List<InstalledProgram> dataList = dataLoader.getData(context);
				
				for (InstalledProgram data : dataList) {
					InstalledProgramViewModel vm = new InstalledProgramViewModel();
					vm.loadFromData(data);
					listData.add(vm);
				}
				
				isLoading = false;
				
				if (listener  != null) listener.finishedLoading(InstalledProgramsListViewModel.this);
			}
		}.start();
	}
	
	public InstalledProgram getByRank(int rank) {
		if (rank >= 0 && rank < this.listData.size()) {
			return this.listData.get(rank).getData();
		}
		return null;	
	}
	
	public int getRankBy(InstalledProgram exProgram) {
		if (exProgram != null) {
			for (int rank = 0; rank < this.listData.size(); rank++) {
				if (this.listData.get(rank).getData().getIpClass().equals(exProgram.getIpClass()) &&
					this.listData.get(rank).getData().getIpPackage().equals(exProgram.getIpPackage())) {
					return rank;
				}
			}
		}
		return 0;	
	}

}
