package com.mrk.btautoplay.viewmodel;

import com.mrk.btautoplay.models.Execution;
import com.mrk.mrkframework.viewmodel.abstracts.AbstractViewModel;

public class ExecutionViewModel extends AbstractViewModel<Execution>{
	
	public ExecutionViewModel() {
		this.data = new Execution();

		this.data.setExBtDeviceAddress("");
		this.data.setExBtDeviceName("");
		this.data.setExDelay(0);
		this.data.setExMaxVolume(true);
		this.data.setExPlay(false);
		this.data.setExProgram(null);
	}
	
	@Override
	public ExecutionViewModel clone() {
		ExecutionViewModel copy = new ExecutionViewModel();
		
		copy.data.setExBtDeviceAddress(this.data.getExBtDeviceAddress());
		copy.data.setExBtDeviceName(this.data.getExBtDeviceName());
		copy.data.setExDelay(this.data.getExDelay());
		copy.data.setExMaxVolume(this.data.getExMaxVolume());
		copy.data.setExPlay(this.data.getExPlay());
		copy.data.setExProgram(this.data.getExProgram());
		
		return copy;
	}
	
}
