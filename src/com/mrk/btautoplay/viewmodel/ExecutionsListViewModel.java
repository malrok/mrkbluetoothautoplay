package com.mrk.btautoplay.viewmodel;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;

import com.mrk.btautoplay.models.Execution;
import com.mrk.mrkframework.dataloaders.interfaces.IDataLoader;
import com.mrk.mrkframework.viewmodel.abstracts.AbstractListViewModel;
import com.mrk.mrkframework.viewmodel.interfaces.IViewModel;

public class ExecutionsListViewModel extends AbstractListViewModel<Execution> {

	@Override
	public void clear() {
		this.listData = new ArrayList<IViewModel<Execution>>();
	}
	
	@Override
	public void loadFromDataLoader(Context context, IDataLoader<Execution> dataLoader) {
		List<Execution> dataList = dataLoader.getData(context);
		
		for (Execution data : dataList) {
			ExecutionViewModel vm = new ExecutionViewModel();
			vm.loadFromData(data);
			this.listData.add(vm);
		}
	}
	
	public void removeVM(ExecutionViewModel vm) {
		IViewModel<Execution> toBeRemoved = null;
		
		if (this.listData.contains(vm)) {
			toBeRemoved = vm;
		} else {
			for (IViewModel<Execution> dataVm : this.listData) {
				if (dataVm.getData().getExBtDeviceAddress().equalsIgnoreCase(vm.getData().getExBtDeviceAddress()) &&
					dataVm.getData().getExProgram().getIpClass().equalsIgnoreCase(vm.getData().getExProgram().getIpClass()) &&
					dataVm.getData().getExProgram().getIpPackage().equalsIgnoreCase(vm.getData().getExProgram().getIpPackage())) {
					toBeRemoved = dataVm;
				}
			}
		}
		
		if (toBeRemoved != null) {
			this.listData.remove(toBeRemoved);
		}
	}
}
